#!/usr/bin/python3

from __future__ import print_function
import pymysql,json, os, sys
import connect_db, time, datetime
from flask import Flask, render_template, request, abort, jsonify, url_for, render_template_string, make_response
from rss_functions import update_feed, insert_feed, get_feed_list_with_count, is_valid_url, generate_tag, remove_tags, import_opml, tdelta, get_total_items, write_output, get_categories, export_opml_file
from werkzeug.utils import secure_filename
from collections import OrderedDict

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = './tmp'
#try:
#reload(sys)
#sys.setdefaultencoding('utf-8')
#except:
   # ;
   # pass;

def get_fancy_time(sec):
    t = datetime.timedelta(seconds = sec)
    if (t.days > 0):
        return str(t.days) + " days"
    elif (sec / 3600) > 1:
        return str(int(sec // 3600)) + " hrs"
    elif (sec / 60) > 1:
        return str(int(sec // 60)) + " mins"
    else:
        return str(sec) + " sec"


@app.route('/fancy_time/<int:sec>', methods=['POST'])
def fancy_time(sec=0):
    try:
        val = str(datetime.timedelta(seconds=sec))
        return json.dumps({'success':True, 'value': val}), 200, {'ContentType':'application/json'}
    except Exception as e:
#        raise e
        return json.dumps({'success':False, 'message': str(e)}), 200, {'ContentType':'application/json'}


@app.route('/getfeed/<int:feedid>/')
def getfeed(feedid):
    try:
        check_feed_exists(feedid)
        db = connect_db.connect_db()
        cur = db.cursor()
        sql = "SELECT *, category.name AS category_name, category.id AS category_id, feeds.tag_colour AS tag_colour FROM feeds LEFT JOIN category ON feeds.category = category.id WHERE feeds.id = %s LIMIT 1"
        cur.execute(sql, feedid)
        row = cur.fetchone()
        row['cleanup_h'] = str(datetime.timedelta(seconds=row['cleanup_period']))
        row['update_h'] = str(datetime.timedelta(seconds=row['update_period']))
        return json.dumps({'success':True, 'result': row}), 200, {'ContentType':'application/json'}
    except Exception as e:
#        raise e
        return json.dumps({'success':False, 'message': str(e)}), 200, {'ContentType':'application/json'}
           
@app.route('/getfeedname/<int:feedid>', methods=['POST'])
def getfeedname(feedid):
    try:
        check_feed_exists(feedid)
        db = connect_db.connect_db()
        cur = db.cursor()
        sql = "SELECT feeds.name FROM feeds LEFT JOIN category ON feeds.category = category.id WHERE feeds.id = %s LIMIT 1"
        cur.execute(sql, feedid)
        row = cur.fetchone()
        return json.dumps({'success':True, 'result': row['name']}), 200, {'ContentType':'application/json'}
    except Exception as e:
#        raise e
        return json.dumps({'success':False, 'message': str(e)}), 200, {'ContentType':'application/json'}
       
@app.route('/addfeed/', methods=['POST'])
def addfeed():
    try:
        name = request.form.get('name')
        url = request.form.get('url')
        update = request.form.get('update')
        cleanup = request.form.get('cleanup')
        feedid = request.form.get('feedid', None)
        tag = request.form.get('tag', '')
        category = request.form.get('category', 1)
        tag_colour = request.form.get('tag_colour')
        cleanup = str(tdelta(cleanup))
        update = str(tdelta(update))
        if not is_valid_url(url) or not update.isdigit() or not cleanup.isdigit():
            raise ValueError('invalid input')

        insert_feed(url, name, update, cleanup, tag, feedid, category, tag_colour)
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}
    except Exception as e:
        #raise e
        return json.dumps({'success':False, 'message': str(e)}), 200, {'ContentType':'application/json'}


@app.route('/')
@app.route('/show/')
@app.route('/show/<feed>/')
@app.route('/show/<feed>/<int:rowcount>/')
def homepage(feed=None, rowcount=50): 
    try:
        #if not request.script_root:
                # this assumes that the 'index' view function handles the path '/'
         #   request.script_root = url_for('', _external=True)
        try :
            int(feed)
            feed_nr = True
        except :
            feed_nr = False
        if feed is None or (feed_nr and int(feed) <= 0):
            feeds = []
            feed = -1
        elif (not feed_nr):
            feeds = [ x  for x in feed.split(',') if x.isdigit() ]
            feed = ','.join(feeds)
        else :
            feeds = [ feed ]
        categories = get_categories()
        return render_template("main.html", item_count = rowcount, feedid=feed, categories = categories)
    except Exception as e:
        #raise e
        return json.dumps({'success':False,  "message": str(e)}), 200, {'ContentType':'application/json'}

@app.route('/getitems')
@app.route('/getitems/<feed>')
@app.route('/getitems/<feed>/<int:start>')
@app.route('/getitems/<feed>/<int:start>/<int:rowcount>')
@app.route('/getitems/<feed>/<int:start>/<int:rowcount>/<int:append>', methods=['POST', 'GET'])
@app.route('/getitems/<feed>/<int:start>/<int:rowcount>/<int:append>/<int:cat>', methods=['POST', 'GET'])
def get_items(feed=None, start=0, rowcount=50, append=0, cat=0):
    try:
        search = request.json.get('search', '').strip()
        ids = request.json.get('ids', [])
        include_read = request.json.get('include_read', False)
        db = connect_db.connect_db()
        cur = db.cursor()
        input_arr = []
        try :
            int(feed)
            feed_nr = True
        except :
            feed_nr = False

        if feed is None or (feed_nr and int(feed) < 0):
            feeds = []
        elif (not feed_nr):
            feeds = [ x  for x in feed.split(',') if x.isdigit() ]
        else :
            feeds = [ feed ]
        format_strings = ','.join(['%s'] * len(feeds))
        try:
            int(cat)
            if cat < 0:
                cat = 0;
        except:
            cat = 0;

        items_q = ''
        if (ids != []) :
            items_q = ' AND (items.id NOT IN (' + (','.join(['%s'] * len(ids))) + ')) '
            input_arr += ids

        cat_q = '';
        if (cat > 0):
            cat_q = ' AND feeds.category = ' + str( cat) + ' '

        sql = "SELECT items.*, feeds.tag AS tag, feeds.name AS feedname, feeds.tag_colour AS tag_colour FROM items LEFT JOIN feeds ON feeds.id = items.feed_id WHERE 1=1 " + items_q + cat_q
        if (include_read == 0) : 
            sql +=  " AND (markedread = 0) "
        if search != '':
            s = [ x.strip() for x in search.split(' ') if x.strip() != '' ]
            for x in s:
                sql += " AND (items.name LIKE %s) "
                input_arr.append("%" + x + "%")

        if len(feeds) > 0:
            sql = sql + ' AND (feed_id IN (' + format_strings + '))'
            sql = sql + ' ORDER BY markedread, publish_date DESC, timestamp DESC LIMIT ' + str(start) + ',' + str(rowcount)
            input_arr += feeds
        else:
            feed = -1
            sql = sql + ' ORDER BY markedread, publish_date DESC, timestamp DESC LIMIT ' + str(start) + ',' + str(rowcount) 
        cur.execute(sql, tuple(input_arr))
        data = []
        
        now = int(time.time())
        for i in cur.fetchall():
            pub_date = i['publish_date']
            if pub_date != 0:
                pub_date = get_fancy_time(now -i['publish_date'])
            else :
                pub_date = ''
            tag = str(i['tag'])
            if tag == '':
                tag = generate_tag(i['feedname'])
            data.append( {'name' : remove_tags(i['name']), 'publish_date': pub_date, 'tag': tag, 'link' : i['link'], 'markedread': i['markedread'], 'id': i['id'], 'summary': remove_tags(i['summary']), 'feedname': i['feedname'], 'tag_colour': i['tag_colour']})
        rhtml = render_template("inner_items_table.html", data = data, append=append)
        
        return json.dumps({ 'success': True, 'result': data, 'last_item' : start + len(data), 'rhtml' : rhtml, 'feedid': feed, 'item_count': rowcount} )
    except Exception as e:
#        raise e
        return json.dumps({'success':False, "message": str(e)}), 201, {'ContentType':'application/json'}

@app.route('/deletecategory/', methods=['POST'])
def deletecategory():
    try: 
        db = connect_db.connect_db()
        cur = db.cursor()
        cat_id = request.json.get('id')
        sql = "DELETE FROM category WHERE id = %s"
        cur.execute(sql, cat_id)
        sql1 = "UPDATE feeds SET category = 1 WHERE category = %s"
        cur.execute(sql1, cat_id)
        db.commit()
        return json.dumps({ 'success': True} ), 200, {'ContentType':'application/json'}
    except Exception as e:
#        raise e
        return json.dumps({'success':False, "message": str(e)}), 201, {'ContentType':'application/json'}



@app.route('/addcategory/', methods=['POST'])
def addcategory():
    try: 
        db = connect_db.connect_db()
        cur = db.cursor()
        cur1 = db.cursor()
        name = request.json.get('name')
        cat_id = request.json.get('id')
        sql = "SELECT count(id) AS cnt FROM category WHERE name = %s"
        cur.execute(sql, name)
        cnt = cur.fetchone()
        if cnt['cnt'] == 0 and cat_id.isnumeric():
            sql = "UPDATE category SET name = (%s) WHERE id = %s"
            i= cur1.execute(sql, (name, cat_id))
        elif cnt['cnt'] == 0:
            sql = "INSERT INTO category (name) VALUES (%s)"
            i= cur1.execute(sql, name)
        else:
            raise ValueError("Feed with that name already exists")
        db.commit()
        return json.dumps({ 'success': True} ), 200, {'ContentType':'application/json'}
    except Exception as e:
#        raise e
        return json.dumps({'success':False, "message": str(e)}), 201, {'ContentType':'application/json'}

@app.route('/getcategories/', methods=['POST'])
@app.route('/getcategories/<style>/', methods=['POST'])
def getcategories(style='list'):
    try:
        cats = get_categories()
        if style == 'list':
            rhtml = render_template("inner_category_list.html", categories = cats)
        else :
            rhtml = render_template("inner_category_selector.html", categories = cats)
        return json.dumps({ 'success': True,  'rhtml' : rhtml} ), 200, {'ContentType':'application/json'}
    except Exception as e:
#        raise e
        return json.dumps({'success':False, "message": str(e)}), 201, {'ContentType':'application/json'}

def check_feed_exists(feedid):
    db = connect_db.connect_db()
    cur = db.cursor()
    sql = "SELECT COUNT(id) AS cnt FROM feeds WHERE id = %s";
    cur.execute(sql, feedid)
    cnt = cur.fetchone()
    
    if cnt['cnt'] == 0:
        raise ValueError("Feed not found")

def check_category_exists(catid):
    db = connect_db.connect_db()
    cur = db.cursor()
    sql = "SELECT COUNT(id) AS cnt FROM category WHERE id = %s";
    cur.execute(sql, catid)
    cnt = cur.fetchone()
    
    if cnt['cnt'] == 0:
        raise ValueError("Category not found")


@app.route('/markfeedread/all/', methods=['POST'])
def markfeedread_all():
    try:
        db = connect_db.connect_db()
        cur = db.cursor()
        sql = "UPDATE items SET markedread = 1"
        cur.execute(sql)
        db.commit()
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}
    except Exception as e:
        return json.dumps({'success':False, 'message': str(e)}), 200, {'ContentType':'application/json'}

@app.route('/markcatread/<int:catid>', methods=['POST'])
def markcatread(catid):
    try:
        check_category_exists(catid)
        db = connect_db.connect_db()
        cur = db.cursor()
        sql = "UPDATE items SET markedread = 1 WHERE feed_id IN (SELECT id FROM feeds WHERE category = %s)"
        cur.execute(sql, catid)
        db.commit()
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}
    except Exception as e:
        return json.dumps({'success':False, 'message': str(e)}), 200, {'ContentType':'application/json'}


@app.route('/markfeedread/<int:feedid>', methods=['POST'])
def markfeedread(feedid):
    try:
        check_feed_exists(feedid)
        db = connect_db.connect_db()
        cur = db.cursor()
        sql = "UPDATE items SET markedread = 1 WHERE feed_id = %s"
        cur.execute(sql, feedid)
        db.commit()
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}
    except Exception as e:
        return json.dumps({'success':False, 'message': str(e)}), 200, {'ContentType':'application/json'}


@app.route('/deletefeed/<int:feedid>', methods=['POST'])
def deletefeed(feedid):
    try:
        check_feed_exists(feedid)
        db = connect_db.connect_db()
        cur = db.cursor()
        sql = "DELETE FROM feeds WHERE id = %s"
        cur.execute(sql, feedid)
        sql = "DELETE FROM items WHERE feed_id = %s"
        cur.execute(sql, feedid)
        db.commit()
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}
    except Exception as e:
        return json.dumps({'success':False, 'message': str(e)}), 200, {'ContentType':'application/json'}


@app.route('/markread/', methods=['POST'])
def mark_read():
    try:
        ids = request.json.get('ids')
        db = connect_db.connect_db()
        cur = db.cursor()
        format_strings = ','.join(['%s'] * len(ids))
        sql = "UPDATE items SET markedread = 1 WHERE id IN (%s) " % format_strings
        cur.execute(sql, tuple(ids))
        db.commit()
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}
    except Exception as e:
        return json.dumps({'success':False, 'message': str(e)}), 200, {'ContentType':'application/json'}


@app.route('/update_feedlist/', methods=['POST'])
def update_feedlist():
    try:
        feeds = get_feed_list_with_count(True)
        categories = get_categories()
        new_feeds = OrderedDict();
        for feed in feeds:
            try :
                new_feeds [ feed['category_id'] ]
            except :
                new_feeds [ feed['category_id'] ] = []

            new_feeds [ feed['category_id'] ].append( feed );
        total_count = get_total_items(True)
        rhtml = render_template("inner_feeds_table.html", feeds = new_feeds, total_count= total_count, categories = categories)
        return json.dumps({'success':True,  'rhtml' : rhtml}), 200, {'ContentType':'application/json'}
    except Exception as e:
        return json.dumps({'success':False, 'message': str(e)}), 200, {'ContentType':'application/json'}


@app.route('/export_opml/', methods=['POST', 'GET'])
def export_opml():
    try:
        opml = export_opml_file()
        response = make_response(render_template_string(opml) )
        response.headers['Content-Type'] = 'application/xml'
        response.headers['Content-Disposition'] = 'attachment; filename="WebRSS.opml'

        return response
    except Exception as e:
#        raise e
        return json.dumps({'success':False, 'message': str(e)}), 200, {'ContentType':'application/json'}

@app.route('/upload_opml/', methods=['POST'])
def upload_opml():
    try:
        data = request.files['filename'].read()
#        fn = secure_filename(filename.filename)
#        fn = os.path.join(app.config['UPLOAD_FOLDER'], fn)
#        print(fn)
#        filename.save(fn)
        import_opml(data)
#        os.unlink(fn)
        return json.dumps({'success':True}), 200, {'ContentType':'application/json'}
    except Exception as e:
        raise e
        return json.dumps({'success':False, 'message': str(e)}), 200, {'ContentType':'application/json'}


if __name__ == "__main__":
    app.jinja_env.trim_blocks = True
    app.jinja_env.lstrip_blocks = True
    app.run(host='0.0.0.0', debug=True)
   
