$( document ).ready(function() {
       
    add_items_lock = 0
    $('body').css('background-image', 'url("' + script_root + '/static/img/necronomicon.png")');
    $('body').css('background-size', 'contain');

    $.clearFormFields = function(area) {
        $('#addfeed_form').find('input[type=text], input[type=password], input[type=number], input[type=email], input[type=url], input[type=hidden], textarea').val('');
        $('#update_h').html('');
        $('#cleanup_h').html('');
    };

    setInterval(update_feed_list, 10101);

    $('#itemstablediv').scrollTop(0);

    $("#addfeed_form").submit(function(event){
        // cancels the form submission
        event.preventDefault();
        submit_addfeed();
        $('#addfeedModal').modal('hide');
        $.clearFormFields('#addfeedModal');
        update_feed_list();

    });

    $("#addcategory_form").submit(function(event){
        // cancels the form submission
        event.preventDefault();
        submit_addcategory();
        update_feed_list();
        edit_category();
        });

    $('#addfeedModal').on('hidden', function(){
      $.clearFormFields(this)
    });
    
    $('#addfeedbutton').click(function() {
        $.clearFormFields('#addfeed_form');
        $.ajax({
            url: script_root + '/getcategories/selector/',
            type: 'POST',
            cache: false,
            contentType: false,
            processData: false
        }).done(function(data) {
            var res = JSON.parse(data);
            $('#feedcategory').html(res.rhtml);
            $('#addfeedModal').modal('show');
            update_feed_update();
            update_feed_cleanup();
            });
        });

    function delete_category(id)
    {
        $.ajax({
            url: script_root + '/deletecategory/',
            type: 'POST',
            cache: false,
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify( { 'id' : id })
        }).done(function(data) {
            var res = JSON.parse(data);
            show_alert_box(data);
            edit_category();
        });
     
    }
    
    $('#editcategory').click(function() {
         edit_category()
    });

    function edit_category() 
    {
        //console.log('edit_catogery');
        $("#category_name").val('');
        $("#category_id").val('');
        $(".edit_category").unbind('click');
        $(".delete_category").unbind('click');

        $("#addcategorybuttontext").show();
        $("#updatecategorybuttontext").hide();
        $.ajax({
            url: script_root + '/getcategories/',
            type: 'POST',
            cache: false,
            contentType: false,
            processData: false
        }).done(function(data) {
            var res = JSON.parse(data);
            //console.log('edit cat');
            $('#category_list').html(res.rhtml);
            $('#editcategoryModal').modal('show');
            $('.edit_category').click( function () { 
                var id = $(this).attr('data-id');
                $("#category_name").val($("#category_"+ id).text());
                $("#category_id").val(id);
                $("#addcategorybuttontext").hide();
                $("#updatecategorybuttontext").show();
            });
            $('.delete_category').click( function () { 
                var id = $(this).attr('data-id');
                delete_category(id);
            });
        });
    }

    $('#proceed_button').click(function() {
        var formData = new FormData($('#upload_opml_form')[0]);
        $.ajax({
            url: script_root + '/upload_opml/',
            type: 'POST',
            data: formData,
            success: function (data) {
                show_alert_box(data);
                $('#upload_modal').modal('hide');
            },
            cache: false,
            contentType: false,
            processData: false
        }).done(function(data) {
            show_alert_box(data);
            $('#upload_modal').modal('hide');
        });

        return false;

    });
    $('#exportopml').click(function() {
        window.open(script_root + "/export_opml/");
    });

    $('#uploadopml').click(function() {
        $('#upload_modal').modal('show');
    });

    function submit_addfeed(){
        // Initiate Variables With Form Content
        var name = $("#feedname").val();
        var url = $("#feedurl").val();
        var cleanup = $("#feedcleanup").val();
        var update = $("#feedupdate").val();
        var tag = $("#tag").val();
        var feedid =  $("#input_feedid").val();
        var category =  $("#feedcategory").val();
        var tag_colour =  $("#tag_colour").val();
        //console.log(category);
        $.ajax({
            type: "POST",
            url: script_root + "/addfeed/",
            data: { 
                'name': name,
                'url': url,
                'tag': tag,
                'cleanup': cleanup,
                'update': update,
                'feedid': feedid,
                'category': category,
                'tag_colour': tag_colour,
                },
        }).done(function(data) {
            show_alert_box(data);
            $('#addfeedModal').modal('hide');
            $.clearFormFields('#addfeedModal');
            update_feed_list();
        });
    }


    function get_feed_name(feedid, elem_id) {
        $.ajax({
            type: "POST",
            url: script_root + "/getfeedname/" + feedid,
        }).done(function(data) {
            var res = JSON.parse(data);
            if (res.success) {
                res = res.result;
                $('#' + elem_id).text(res);
            } else {
                show_alert_box(data);
            }
        });
    }
    function submit_addcategory() {
        var name = $("#category_name").val();
        var id = $("#category_id").val();
        $.ajax({
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            url: script_root + "/addcategory/",
            data: JSON.stringify( { 'name' : name, 'id' : id })
        }).done(function(data) {
            show_alert_box(data);
        });
    }

    function set_deletefeed_handler() {
        $("[id^='delfeed_']").click(function(event) {
            var feedid = $(this).attr("id").replace('delfeed_', '');
            get_feed_name(feedid,'feedname_delete');
            $('#delete_modal').modal('show');
            $('#delete_button').click(function(event) {
                $.ajax({
                    type: "POST",
                    url: script_root + "/deletefeed/" + feedid,
                }).done(function(data) {
                    show_alert_box(data);
                    $('#delete_modal').modal('hide');
                    update_feed_list();
                    update_items_list();
                });

            });
        });
    }


    function set_openfeed_handler() {
        $("[id^='openfeed_']").click(function(event) {
            var feedid = $(this).attr("id").replace('openfeed_', '');
            if (feedid == '') { feedid = -1; }
            $('#searchbar').val('');
            add_items(feedid, 0, 50, true);
        });
        $("[id^='opencat_']").click(function(event) {
            console.log('caoeua');
            var catid = $(this).attr("id").replace('opencat_', '');
            console.log('caoeua', catid);
            if (catid == '') { return -1; }
            $('#searchbar').val('');
            add_items(-1, 0, 50, true, catid);
        });
    }

    function set_openitem_handler() {
        $(".itemlink").click(function(event) {
            event.stopPropagation();
            var url= $(this).attr("data-url");
            var id= $(this).attr("data-id");
            window.open(url,'_blank');
            mark_read([ id ], false)
            $(this).removeClass("bold");
        });
    }

    function update_feed_update()
    {
        $('#feedupdate').on('input', function() {
            $.ajax({
                url: script_root + '/fancy_time/' + $('#feedupdate').val(),
                type: 'POST',
                cache: false,
                contentType: false,
                processData: false
            }).done(function(data) {
                var res = JSON.parse(data);
                $('#update_h').val(res.value);
            });
        });
    }

    function update_feed_cleanup()
    {
        $('#feedcleanup').on('input', function() {
            $.ajax({
                url: script_root + '/fancy_time/' + $('#feedcleanup').val(),
                type: 'POST',
                cache: false,
                contentType: false,
                processData: false
            }).done(function(data) {
                var res = JSON.parse(data);
                $('#cleanup_h').val(res.value);
            });
        });

    }
    function set_editfeed_handler() {
        $("[id^='editfeed_']").click(function(event) {
            var feedid = $(this).attr("id").replace('editfeed_', '');
            $("#input_feedid").val(feedid);
            $.ajax({
                type: "GET",
                url: script_root + "/getfeed/" + feedid  + '/'
            }).done(function(data) {
                var res = JSON.parse(data);
                if (res.success) {
                    update_feed_update();
                    update_feed_cleanup();
                    
                    res = res.result;
                    ///console.log(res);
                    var category_id = res.category_id;
                    $('#feedname').val(res.name);
                    $('#feedurl').val(res.url);
                    $('#tag').val(res.tag);
                    $('#update_h').val(res.update_h);
                    $('#feedupdate').val(res.update_period);
                    $('#cleanup_h').val(res.cleanup_h);
                    $('#feedcleanup').val(res.cleanup_period);
                    $('#tag_colour').val(res.tag_colour);
                    $('#example_badge').css('background-color', res.tag_colour);
                    $("#addfeedbuttontext").hide();
                    $("#updatefeedbuttontext").show();
                    //console.log(res.category_name, res.category_id );
                    $.ajax({
                        url: script_root + '/getcategories/selector/',
                        type: 'POST',
                        cache: false,
                        contentType: false,
                        processData: false
                    }).done(function(data) {
                        var res = JSON.parse(data);
                        $('#feedcategory').html(res.rhtml);
                        $('#feedcategory option').each( function() { $(this).removeAttr('selected'); });
                        $('#feedcategory option[value=' + category_id + ']').attr('selected','selected');
                        $('#feedcategory option').each( function() { 
                            if ($(this).val() == category_id) { $(this).attr('selected', 'selected'); }
                        //    console.log($(this).attr('selected'), $(this).text(), $(this).val(), category_id); 
                        });
                        $('#addfeedModal').modal('show');
                    });
                } else {
                    show_alert_box(data);
                }

            });       
        });
    }

            
    $("#markread").click(function(event) {
        mark_read(item_ids, true);
    });
        
        
    function mark_read(ids, update_items) 
    {
        $.ajax({
            type: "POST",
            url: script_root + "/markread/",
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify({ "ids": ids })
        }).done(function(data) {
            update_feed_list();
            if (update_items) {
                update_items_list();
            }
        });       
    }

    function show_alert_box(data, suppress_success)
    {
        var res = JSON.parse(data);
        if (res.success == true) {
            if (suppress_success != true) { 
                $('#alert_success_text').text("Success");
                $('#alert_success_bar').show();
                setTimeout(function() {$('#alert_success_bar').hide()}, 4000);
                $('#alert_success_bar').click( function () { $('#alert_success_bar').hide();} );
            }
        } else {
            $('#alert_error_text').text("Error: " + res.message);
            $('#alert_error_bar').show();
            setTimeout(function() {$('#alert_error_bar').hide()}, 4000);
            $('#alert_error_bar').click( function () { $('#alert_error_bar').hide();} );
        }
    }

    function set_markfeed_handler() 
    {
        $("[id^='markfeed_']").click(function(event) {
            var feedid = $(this).attr("id").replace('markfeed_', '');
            $.ajax({
                type: "POST",
                url: script_root + "/markfeedread/" + feedid,
                contentType: "application/json;charset=UTF-8",
            }).done(function(data) {
                show_alert_box(data);
                update_feed_list();
                update_items_list();

            });       
        });   
        $("[id^='markcat_']").click(function(event) {
            var catid = $(this).attr("id").replace('markcat_', '');
            $.ajax({
                type: "POST",
                url: script_root + "/markcatread/" + catid,
                contentType: "application/json;charset=UTF-8",
            }).done(function(data) {
                show_alert_box(data);
                update_feed_list();
                update_items_list();

            });       
        });
        $("#markread_allfeeds").click(function(event) {
            $.ajax({
                type: "POST",
                url: script_root + "/markfeedread/all/",
                contentType: "application/json;charset=UTF-8",
            }).done(function(data) {
                show_alert_box(data);
                update_feed_list();
                update_items_list();

            });       
        });
    }

    function set_scroll_handler(id, fn)
    {
        $(id).scroll(function() {
            var scrollPosition = $(id).scrollTop() + $(id).innerHeight();
            var divTotalHeight = $(id).get(0).scrollHeight +
                parseInt($(id).css('padding-top'), 10) +
                parseInt($(id).css('padding-bottom'), 10);

            if ((scrollPosition + 1) >= divTotalHeight) {
                fn();
            }
        });
    }

    function add_items_handler()
    {
        add_items(current_feed,last_item, item_count, false, current_cat);
    }

    function add_items(current_feed_in,last_item_in, item_count_in, clear, category)
    {
        var include_read = $("#readitems").prop("checked");
        var search = $("#searchbar").val();
        var append = 0;
        if (add_items_lock > 0) {
            setTimeout(function() {add_items(current_feed_in,last_item_in, item_count_in, clear, current_cat)}, 500);
            return;
        }
        add_items_lock = 1;
        if (!clear)  { 
            append = 1; 
        }
        if (clear) {
            item_ids = [];
        }
        if (category == undefined) {
            category = 0;
        }
        $.ajax({
            type: "POST",
            url: script_root + "/getitems/" + current_feed_in + "/0/" + item_count_in + "/" + append + "/" + category,
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify({'include_read': include_read?1:0, "search": search, "ids": item_ids } )
        }).done(function(data) {
            show_alert_box(data, true);
            var res = JSON.parse(data);
            if (clear) {
                $('#itemstablediv').scrollTop(0);
                current_feed = res.feedid;
                item_count = res.item_count;
                current_cat = category;
                $('#itemstable>tbody').html(res.rhtml);
            } else {
                $('#itemstable>tbody tr').eq(-1).after(res.rhtml);
            }
            for (var i = 0; i < res.result.length ; i++) {
                item_ids.push(res.result[i].id);
            }
            last_item = res.last_item;
            set_editfeed_handler();
            set_feed_handlers();
            set_item_handlers();
            add_items_lock = 0;
        });
    }

    var category_set = [];

    function set_feed_handlers()
    {
        $("[id^='editfeed_']").unbind('click');
        $("[id^='openfeed_']").unbind('click');
        $("[id^='opencat']").unbind('click');
        $("[id^='markfeed_']").unbind('click');
        $("[id^='markcat']").unbind('click');
        $("[id^='deletefeed_']").unbind('click');
        $("#markread_allfeeds").unbind('click');
        $(".category_selector").unbind('click');
        set_editfeed_handler();
        set_openfeed_handler();
        set_markfeed_handler();
        set_deletefeed_handler();
        $(".category_selector").click(function() {
            var id = $(this).attr('data-id');
            $(this).find('.cat_plus').toggle(); 
            $(this).find('.cat_minus').toggle();
            $('.category_toggle_' + id).toggle();
            if ($(this).find('.cat_minus').is(':hidden')) { 
                category_set[id] = 1 ;
            } else { 
                category_set[id] = 0;
            }
        });
        category_set.forEach( function(item, id) { 
            if (item == 1) { // cat_minus is hidden
                $('.category_toggle_' + id).hide();
                $('.category_selector_' + id).find('.cat_minus').hide(); 
                $('.category_selector_' + id).find('.cat_plus').show(); 
            } else {
                $('.category_toggle_' + id).show();
                $('.category_selector_' + id).find('.cat_minus').show(); 
                $('.category_selector_' + id).find('.cat_plus').hide(); 
            }
        });
    }
    function set_item_handlers()
    {
         $(".itemlink").unbind('click');
         set_openitem_handler();
         $("#itemstable>tbody tr>td>div").unbind('click');
         $("#itemstable>tbody tr>td>div").click(function(e) { $(this).next('div').toggle(); });
    }

    $("#itemstable>tbody tr").click(function(e) {
        $(this).next('div').toggle();
    });

    set_scroll_handler("#itemstablediv", add_items_handler);

    function update_feed_list()
    {
        $.ajax({
            type: "POST",
            url: script_root + "/update_feedlist/",
        }).done(function(data) {
            show_alert_box(data, true);
            var res = JSON.parse(data);
            $("#feed_list_ul").html(res.rhtml);
            set_feed_handlers();
        });
    }

    function update_items_list()
    {
         add_items(current_feed, 0, 50, true, current_cat)
         set_item_handlers();
    }

    update_feed_list();
    update_items_list();

    $("#searchbutton").click(function(){
         add_items(current_feed, 0, 50, true, current_cat)
    });


    $("#searchbar").keypress(function(event){
        var keycode = event.keyCode || event.which;
        if(keycode == '13') {
             add_items(current_feed, 0, 50, true, current_cat)
        }
    });

    $('#hidefeeds').click(function() { 
        $('#feed_list_bar').toggle(); 
        $('.left_menu').toggle(); 
        $('.right_menu').toggle();
        
    });

    $('#tag_colour').change(function() {
       var colour = $(this).val().replace(/[^a-zA-Z0-9#]/g, '');
       //console.log(colour);
       $('#example_badge').css('background-color', colour);
    });


});
