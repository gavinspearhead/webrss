#!/usr/bin/python3

import pymysql
import connect_db, sys, os, re
import feedparser, time, syslog
import urllib3
import requests
import threading, unicodedata, traceback
from xml.etree import ElementTree
from multiprocessing import JoinableQueue
from collections import OrderedDict
from io import BytesIO
from datetime import datetime


blacklist = [ "https://srv.buysellads.com" ]

try:
    # Python 2.6-2.7 
    from HTMLParser import HTMLParser
except ImportError:
    # Python 3
    from html.parser import HTMLParser

try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse

DEFAULT_UPDATE_PERIOD = 900  ## 0.5 hours
DEFAULT_CLEANUP_PERIOD = 4 * 7 * 24 * 3600 # 4 weeks

q = None


def write_output(string, priority=syslog.LOG_INFO):
   syslog.syslog(priority, string) 
#   print(string) 


def tdelta(input):
    input = str(input).strip()
    if str(input).isdigit():
        return int(input)
    keys = ['years','Months', "weeks", "days", "hours", "minutes", 'seconds']
    tkeys = [365*24*3600, 4*7*24*3600, 7*24*3600, 24*3600, 3600, 60, 1]
    regex = "".join(["((?P<%s>\d+)%s ?)?" % (k, k[0]) for k in keys])
    kwargs = {}
    for k,v in re.match(regex, input).groupdict(default="0").items():
        kwargs[k] = int(v)

    td = 0
    for i in range(len(keys)):
        td += kwargs[ keys[ i] ] * tkeys[i]
        
    return td


def is_valid_url(x):
    try:
        result = urllib3.util.parse_url(x)
        return True if [result.scheme, result.netloc, result.path] else False
    except Exception as e:
        return False


def get_total_items(marked):
    try:
        db = connect_db.connect_db()
        cur = db.cursor()
        mark_sql = ''
        if marked:
            mark_sql = ' AND markedread = 0 '
        sql = "SELECT count(items.id) AS cnt FROM items WHERE 1=1 " + mark_sql 
        cur.execute(sql)
        counter = cur.fetchone()['cnt']
        db.close()
        return counter
    except Exception as e:
        write_output(str(e), syslog.LOG_ERR)
        return 0


def get_feed_list_with_count(marked):
    try:
        db = connect_db.connect_db()
        cur = db.cursor()
        mark_sql = ''
        if marked:
            mark_sql = ' AND markedread = 0 '
        sql = "SELECT feeds.id, feeds.name, count(items.id) AS cnt, feeds.error AS err, category.name as category_name, category.id category_id FROM feeds LEFT JOIN category ON feeds.category = category.id LEFT JOIN items ON feeds.id = items.feed_id " + mark_sql + " GROUP BY feeds.id ORDER BY category.name, feeds.name"
        cur.execute(sql)
        feeds = []
        for i in cur.fetchall():
            feeds.append( { 'name': i['name'], 'category_name': i['category_name'], 'category_id' : i['category_id'], 'id': i['id'], 'cnt': i['cnt'], 'err': i['err'] } )
        db.close()
        return feeds;
    except Exception as e:
        write_output(str(e), syslog.LOG_ERR)
        return [] 

def get_feed_urls():
    try:
        db = connect_db.connect_db()
        cur = db.cursor()
        sql = "SELECT * FROM feeds ORDER BY feeds.name"
        cur.execute(sql)
        feeds = []
        for i in cur.fetchall():
            feeds.append( { 'name': i['name'], 'id': i['id'], 'url': i['url'] } )
        db.close()
        return feeds;
    except Exception as e:
        write_output(str(e), syslog.LOG_ERR)
        return [] 


def get_feed_list():
    try:
        db = connect_db.connect_db()
        cur = db.cursor()
        sql = "SELECT * FROM feeds ORDER BY feeds.name"
        cur.execute(sql)
        feeds = []
        for i in cur.fetchall():
            feeds.append( { 'name': i['name'], 'id': i['id'] } )
        db.close()
        return feeds;
    except Exception as e:
        write_output(str(e), syslog.LOG_ERR)
        return [] 

def read_feed_from_url(url):
    try:
        resp = requests.get(url, timeout=20.0)
    except requests.ReadTimeout:
        logging.warn("Timeout when reading RSS {}".format(url))
        raise ValueError("Timeout when reading RSS {}".format(url))

    # Put it to memory stream object universal feedparser
    content = BytesIO(resp.content)

    # Parse content
    feed = feedparser.parse(content)
    return feed


def update_feed(feed_id=None, url=None, force=False):
    try:
        db = connect_db.connect_db()
        cur = db.cursor()
        if force: 
            now = int(time.time()) + (1000 * 365 * 24 * 3600) ## add thousand years
        else :
            now = int(time.time())
        if feed_id is not None:
            sql = "SELECT url, id FROM feeds WHERE id = %s AND (last_updated + update_period) < %s LIMIT 1"
            cur.execute(sql, (feed_id, now))
        elif url is not None:
            sql = "SELECT url, id FROM feeds WHERE url = %s AND (last_updated + update_period) < %s LIMIT 1"
            cur.execute(sql, (url, now))
        else:
            raise ValueError("Missing parameter feedid or url: " + url)
        row = cur.fetchone()
        counter = 0
        if row is None:
            return counter
        url = row['url']
        feed_id = row['id']
        # print(url)
        try :

            # feed = feedparser.parse(url)
            feed = read_feed_from_url(url)
            main_link = feed.feed.link
            if feed.get('bozo', 0) == 1 and len(feed.get('entries', {})) == 0:
                #print(feed, url)
                raise ValueError('Feed could not be parsed correctly: {}'.format(url))
        except Exception as e:
            # traceback.print_exc()

            # print(e)
            write_output("Error: reading feed {0} {1}".format(url, str(e)))
            # print(e)
            now = int(time.time())
            sql = "UPDATE feeds SET last_updated = %s, error = 1 WHERE id = %s"
            # print(now, feed_id)
            cur.execute(sql, (now, feed_id))
            db.commit()
            # print('aoeuae')
            return 0 
        now = int(time.time())
        for i in feed['items']:
            summary = i.get('summary', '')
            summary = unicodedata.normalize("NFKC",summary).strip()
            link = i.link.strip()
            if not (link.lower().startswith('https://') or link.lower().startswith('http://')) and main_link != "":
                link = main_link + link
            title = i.get('title', '').strip()
            blacklisted = False
            for l in blacklist:
                if link.find(l) >= 0:
                    blacklisted = True
                    break
            if blacklisted:
                continue
            try :
                publish_date = int(time.mktime(i['published_parsed']))
            except (KeyError, TypeError) as e:
                try:
                    publish_date = int(time.mktime(i['updated_parsed']))
                except (KeyError, TypeError) as e:
                    try :
                        publish_date = int(time.mktime(i['created_parsed']))
                    except (KeyError, TypeError) as e:
                        publish_date = 0
            if publish_date > now:
                publish_date = now
            if 0 < publish_date < (now - 3600 * 24 * 21): # older than 21 days
#                write_output('Too old article')
                continue
            if link == '' or title == '':
                write_output('Missing link or title')
                continue;
            sql = "SELECT count(*) AS cnt FROM items WHERE feed_id = %s AND link = %s"
            cur.execute(sql, (feed_id, link.encode('utf8')))
            cnt = cur.fetchone()['cnt']
            # print(feed_id, link, cnt) 
            if cnt == 0:
                try :
                    sql = "INSERT INTO items (timestamp, link, summary, feed_id, name, markedread, publish_date) VALUES (%s,%s,%s,%s,%s,%s,%s)"
                    cur.execute(sql, (now, link.encode('utf8'), summary.encode('utf8'), feed_id, title.encode('utf8'), 0, publish_date))
                    db.commit()
                    counter += 1
                except Exception as e: 
                    #print(e)
                    continue;

        sql = "UPDATE feeds SET last_updated = %s, error = 0 WHERE id = %s"
        cur.execute(sql, (now, feed_id))
        db.commit()
        db.close()
        return counter

    except Exception as e:
#        print(e)
        try :
            sql = "UPDATE feeds SET last_updated = %s, error = 1 WHERE id = %s"
            cur.execute(sql, (now, feed_id))
            db.commit()
            db.close()
        except Exception:
            pass
        write_output(str(e), syslog.LOG_ERR)
        return 0


def handle_update():
    while True:
        try:
            if q.empty():
                break
            X = q.get()
            if X is None:
                break

            (feed_id, name, force) = X
            write_output('starting update: {0} {1} {2}'.format(name, feed_id, force))
            cnt = update_feed(feed_id=feed_id, force=force)
            write_output('updated: {0} {1}'.format(name, cnt) )
        except Exception as e:
            write_output(str(e))
        finally:
            q.task_done()

    
def update_all_feeds(threads, force=False):
    global q
    if threads < 1:
        raise ValueError("Threads must be at least one")
    q = JoinableQueue()
    feeds = get_feed_list()
    for f in feeds:
        q.put((f['id'], f['name'], force))

    #write_output('queuesize {0}'.format(q.qsize()))
    for i in range(threads):
        t = threading.Thread(target = handle_update)
        t.daemon = True
        t.start()
    q.join()
    q = None


def update_all_feeds_sync(force=False):
    try:
        write_output('Starting synchronous update', syslog.LOG_DEBUG)
        feeds = get_feed_list()
        for f in feeds:
            cnt = update_feed(feed_id=f['id'], force=force)
            #write_output('updated: {0} {1}'.format(f['name'], cnt) )
    except Exception as e:
        write_output(str(e), syslog.LOG_ERR)


def insert_feed(url, name, update_period=DEFAULT_UPDATE_PERIOD, cleanup_period = DEFAULT_CLEANUP_PERIOD, tag='', feedid=None, category=1, tag_colour="#0000DD"):
    try:
        db = connect_db.connect_db()
        cur = db.cursor()
        name = name.strip()
        url = url.strip()
        tag = tag.strip()
        tag_colour = re.sub('[^A-Za-z0-9#]', '', tag_colour).strip();
        if not is_valid_url(url):
            raise ValueError ("No valid URL: " + url)
        if name == '':
            res = urllib3.util.url.parse_url(url)
            name = res.netloc + "_" + (res.path.replace("/", "_"))
        if feedid is not None and feedid.isdigit() and int(feedid)>0:
            sql = "UPDATE feeds SET name = %s, last_updated = %s, update_period = %s, cleanup_period = %s, error = 0, url = %s, tag = %s, category = %s, tag_colour = %s WHERE id = %s"
            cur.execute(sql, (name, 0, update_period, cleanup_period, url, tag,  category, tag_colour, feedid))
            #print(sql, (name, 0, update_period, cleanup_period, url, tag, feedid, category), cur._last_executed)
        else:
            sql = "SELECT count(*) AS cnt FROM feeds WHERE url = %s";
            cur.execute(sql, (url))
            cnt = cur.fetchone()['cnt']

            if cnt == 0:
                sql = "INSERT INTO feeds (url, name, last_updated, update_period, cleanup_period, error, tag, category, tag_colour ) VALUES (%s,%s,%s,%s, %s, 0, %s, %s, %s)"
                cur.execute(sql, (url, name, 0, update_period, cleanup_period, tag, category, tag_colour))
            else:
                sql = "UPDATE feeds SET name = %s, last_updated = %s, update_period = %s, cleanup_period = %s, error = 0, tag= %s, category = %s, tag_colour = %s WHERE url = %s"
                cur.execute(sql, (name, 0, update_period, cleanup_period, tag, category, tag_colour, url))
            #print(sql)
        db.commit()
        db.close()
    except Exception as e:
        #print(str(e))
        write_output(str(e))
        raise (e)


def cleanup_feed(feed_id):
    try:
        db = connect_db.connect_db()
        cur = db.cursor()
        sql = "SELECT cleanup_period FROM feeds WHERE id = %s LIMIT 1"
        cur.execute(sql, (feed_id))
        period = cur.fetchone()['cleanup_period']
        if (period == 0) :
            return;

        now = int(time.time())
        sql = "DELETE FROM items WHERE feed_id = %s AND timestamp < %s"
        cur.execute(sql, (feed_id, now - period))
        db.commit()
        db.close()
    except Exception as e:
        write_output(str(e))


def cleanup_all_feeds():
    feeds = get_feed_list()
    for f in feeds:
        cleanup_feed(feed_id=f['id'])
        #write_output('cleaning: {0}'.format( f['name']))
    

def generate_tag(name):
    tag = ''.join(t for t in name if t.isalnum())
    tag = tag[0:6].upper()
    return tag


def import_opml(data):
    try:
        tree = ElementTree.fromstring(data)
        for node in tree.findall('.//outline'):
            url = node.attrib.get('xmlUrl')
            name = node.attrib.get('text')
            if url:
                tag = generate_tag(name)
                insert_feed(url, name, tag = tag)
    except Exception as e:
        write_output(str(e))


def import_opml_file(filename):
    try:
        with open(filename, 'rt') as f:
            tree = ElementTree.parse(f)
        for node in tree.findall('.//outline'):
            url = node.attrib.get('xmlUrl')
            name = node.attrib.get('text')
            if url:
                tag = generate_tag(name)
                insert_feed(url, name, tag = tag)
    except Exception as e:
        write_output(str(e))


def export_opml_file():
    try:
        data = ""
        feeds = get_feed_urls()
        dt = datetime.now()
        now = dt.strftime("%c")
        data += """<?xml version="1.0" encoding="ISO-8859-1"?>
<opml version="2.0">
    <head>
        <title>WebRSS opml</title>
        <dateCreated>{0}</dateCreated>
        <dateModified>{1}</dateModified>
</head>
<body>
"""
        data = data.format(now,now)
        for i in feeds:
            data += "<outline text=\"{}\" description=\"\" htmlUrl=\"\" language=\"unknown\" title=\"{}\" type=\"rss\" version=\"RSS2\" xmlUrl=\"{}\"/>\n".format(i['name'], i['name'], i['url'])

        data += """
    </body>
</opml>
"""
        return data
    except Exception as e:
        write_output(str(e))



def select_update_period(max_sleep_period):
    try:
        db = connect_db.connect_db()
        cur = db.cursor()
        sql = "SELECT MIN(update_period + last_updated) AS minupdate FROM feeds WHERE error = 0"
        now = int(time.time())
        cur.execute(sql)
        period = cur.fetchone()['minupdate']
        if period is None:
            period = max_sleep_period
        
        period = max(0, min(period - now, max_sleep_period))
        db.close()
        return period
    except Exception as e:
        write_output(str(e))


def get_categories():
    try:
        db = connect_db.connect_db()
        cur = db.cursor()
        cur1 = db.cursor()
#        sql = "SELECT id, name, count(*) FROM category LEFT JOIN items ON category.id = items.ORDER BY name"
#        sql = "SELECT COUNT(*) AS count_items, category.id, category.name FROM category LEFT JOIN feeds ON category.id = feeds.category LEFT JOIN items ON items.feed_id = feeds.id WHERE markedread = 0 GROUP BY category.id ORDER BY category.id"
        sql = "SELECT id, name FROM category ORDER BY name"
        cur.execute(sql)
        cats = OrderedDict()
        for i in cur.fetchall():
            sql1 = "SELECT count(*) AS count_items FROM items LEFT JOIN feeds ON items.feed_id = feeds.id WHERE markedread = 0 AND category = %s "
            cur1.execute(sql1, i['id'])
            count_items = cur1.fetchone()
            cats [i['id']] = {'name' :i['name'], 'id' :i['id'], 'count' : count_items['count_items'] }
        db.close()
        return cats
    except Exception as e:
        write_output(str(e))
        raise e


def remove_tags(s):
    h = HTMLParser()
    s = h.unescape(str(s))
    tag = False
    quote = False
    out = ""

    for c in s:
        if c == '<' and not quote:
            tag = True
        elif c == '>' and not quote:
            tag = False
        elif (c == '"' or c == "'") and tag:
            quote = not quote
        elif not tag:
            out = out + c

    return out

