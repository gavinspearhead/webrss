#!/usr/bin/python3

import argparse
import daemonize
import feedparser, time, syslog
from rss_functions import update_feed, insert_feed, update_all_feeds, cleanup_all_feeds, import_opml, select_update_period, update_all_feeds_sync, write_output


MAX_SLEEP_PERIOD = 93 # ~1.5 minute
THREAD_COUNT = 1


def rss_update(max_sleep_time, threads):
    write_output('starting {0} {1}'.format(max_sleep_time, threads))

    while True:
        try:
            period = select_update_period(max_sleep_time)
            force_update = False
            # write_output('sleeping {0} seconds'.format(period))
            time.sleep(period)
            if threads == 1:
                update_all_feeds_sync(force_update)
            else :
                update_all_feeds(threads, force_update)
            cleanup_all_feeds()
        except Exception as e:
            write_output(str(e))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="RSS update daemon")
    parser.add_argument("-d", '--daemon', help="Daemonize process ", action='store_true' )
    parser.add_argument("-f", '--foreground', help="Run process in foreground", action='store_true' )
    parser.add_argument("-o", '--runonce', help="Force update of all feeds", action='store_true' )
    parser.add_argument("-s", '--sleeptime', type=int, help="Maximum polling time between updates", default=MAX_SLEEP_PERIOD, metavar="SECONDS" )
    parser.add_argument("-t", '--threads', type=int, help="Maximum number of threads", default=THREAD_COUNT, metavar="THREADS" )
    parser.add_argument("-i", '--id', type=int, help="Update only feed with id", metavar="id" )
    parser.add_argument("-p", '--pidfile',  help="Location of the PID file", default="/var/run/rss_update.pid", metavar="PIDFILE")
    parser.add_argument("-u", '--user',  help="Drop privileges to this user", default="www-data", metavar="USER")
    args = parser.parse_args()
    
    if (args.runonce):
        update_all_feeds_sync(True)

    elif (args.id):
        update_feed(args.id, force=True)

    elif (args.foreground):
        daemon = daemonize.Daemonize(app="rss_update", pid=args.pidfile, action=lambda: rss_update (args.sleeptime, args.threads), verbose=True, foreground=True, user=args.user)
        daemon.start()
    elif (args.daemon):
        daemon = daemonize.Daemonize(app="rss_update", pid=args.pidfile, action=lambda: rss_update (args.sleeptime, args.threads), verbose=True, foreground=False, user=args.user)
        daemon.start()
    else:
        rss_update(args.sleeptime, args.threads)
